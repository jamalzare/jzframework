//
//  Extension.swift
//  JZFrameWork
//
//  Created by Jamal on 5/4/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

//MARK: - Helper Methods for base data classess
extension Double{
    func annonateWithDot() -> String {
        let int = Int(self)
        let str = String(int)
        var secondStr = ""
        var count = 0
        for index in (0 ... str.count - 1).reversed(){
            if ( count > 0 && count % 3 == 0 )
            {
                secondStr.insert(".", at: secondStr.startIndex)
            }
            let currentIndex = str.index(str.startIndex, offsetBy: index)
            secondStr.insert(str[currentIndex], at: secondStr.startIndex)
            count += 1
            
        }
        return secondStr
    }
}


//MARK: - Design
extension UIView{
    
    func layoutCircle(){
        layer.masksToBounds = true
        layer.cornerRadius = bounds.height/2
    }
    
    func setBorder(color: UIColor = .black, width: CGFloat = 1){
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
    
    @objc dynamic public func shadow(opacity:Float = 0.15, cornerRadius:CGFloat = 4, shadowRadius:CGFloat = 1) {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = opacity
        layer.shadowRadius = shadowRadius
        layer.shadowOffset = CGSize(width: 0, height: 1)
        
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
        
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}

extension CALayer{
    func sketchShadow(
        color: UIColor = .black,
        alpha: Float = 1.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0)
    {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
        
        shouldRasterize = true
        rasterizationScale = UIScreen.main.scale
    }
}

extension UICollectionViewCell{
    
    public override func shadow(opacity:Float = 0.15, cornerRadius:CGFloat = 4, shadowRadius:CGFloat = 1) {
        
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = opacity
        layer.shadowRadius = shadowRadius
        layer.shadowOffset = CGSize(width: 0, height: 1)
        
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
        
        layer.shouldRasterize = false
        layer.rasterizationScale = UIScreen.main.scale
        
        contentView.layer.masksToBounds = true
        contentView.layer.cornerRadius = cornerRadius
        
        //        let shadowLayer = CAShapeLayer()
        //        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        //        shadowLayer.fillColor = UIColor.clear.cgColor
        //
        //        shadowLayer.shadowColor = UIColor.black.cgColor
        //        shadowLayer.shadowPath = shadowLayer.path
        //        shadowLayer.shadowOffset = CGSize(width: 1, height: 1)
        //        shadowLayer.shadowOpacity = 1
        //        shadowLayer.shadowRadius = 4
        //        layer.cornerRadius = cornerRadius
        //        clipsToBounds = true
        //        layer.insertSublayer(shadowLayer, at: 2)
        
        //        let shadowLayer = CAShapeLayer()
        //        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 12).cgPath
        //        shadowLayer.fillColor = UIColor.white.cgColor
        //
        //        shadowLayer.shadowColor = UIColor.black.cgColor
        //        shadowLayer.shadowPath = shadowLayer.path
        //        shadowLayer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        //        shadowLayer.shadowOpacity = 0.8
        //        shadowLayer.shadowRadius = 2
        //
        //        layer.insertSublayer(shadowLayer, at: 0)
        //        layer.masksToBounds = true
    }
}

//MARK: Colors
extension UIColor{
    
    class var dimColor: UIColor{
        return UIColor(white: 0, alpha: 0.5)
    }
    
    class var appLightGreen:UIColor{
        return UIColor(hexString : "00EB8C")
    }
    
    class var appLightGray:UIColor{
        return UIColor.rgb(197, 197, 197)
    }
    
    class var menuColor: UIColor{
        return UIColor(hexString: "F3F3F3")
    }
    
    class func rgb(_ red:CGFloat, _ green:CGFloat, _ blue:CGFloat)->UIColor{
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    class func rgba(_ red:CGFloat, _ green:CGFloat, _ blue:CGFloat, _ alpha:CGFloat)->UIColor{
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
    public convenience init(hexString: String) {
        var cString:String = hexString.uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            self.init()
        }else{
            
            let rString = (cString as NSString).substring(to: 2)
            let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
            let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
            
            var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
            Scanner(string: rString).scanHexInt32(&r)
            Scanner(string: gString).scanHexInt32(&g)
            Scanner(string: bString).scanHexInt32(&b)
            
            self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
        }
    }
}

//Mark: GestureRecognizer
extension UIView {
    
    fileprivate static var tapGestureRecognizerKey = "AssociatedObjectKeyForTabGestureAndUIView"
    
    fileprivate typealias OnTap = ((_ sender: Any) -> Void)?
    
    fileprivate var tapGestureRecognizerOnTap: OnTap? {
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &UIView.tapGestureRecognizerKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            return objc_getAssociatedObject(self, &UIView.tapGestureRecognizerKey) as? OnTap
        }
    }
    
    public func onTap(onTap: ((_ sender: Any) -> Void)?) {
        isUserInteractionEnabled = true
        tapGestureRecognizerOnTap = onTap
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        tapGestureRecognizer.cancelsTouchesInView = false
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let onTap = self.tapGestureRecognizerOnTap {
            onTap?(sender)
        } else {
            print("no setted yet")
        }
    }
    
}



//MARK: CollectionView registration
extension UICollectionViewCell{
    
    static var identifier: String{
        return NSStringFromClass(self)
    }
    
    static var nib: UINib{
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
}

extension UICollectionView{
    
    func register<T: UICollectionViewCell>(_ cell: T.Type) {
        
        register(cell.nib, forCellWithReuseIdentifier: cell.identifier)
    }
    
    func registerHeader<T: UICollectionViewCell>(_: T.Type) {
        
        register(T.nib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: T.identifier)
    }
    
    func registerFooter<T: UICollectionViewCell>(_: T.Type) {
        
        register(T.nib, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: T.identifier)
    }
    
    
    func dequeueCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T  {
        
        register(T.self)
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as? T else {
            fatalError("failed to dequeue \(T.self)")
        }
        return cell
    }
    
    func dequeueHeader<T: UICollectionViewCell>(for indexPath: IndexPath) -> T  {
        
        guard let header = dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: T.identifier, for: indexPath) as? T else{
            fatalError("failed to dequeue \(T.self)")
        }
        return header
    }
}


//MARK: BaseClasses

protocol CollectionViewDelegates: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
}

class View: UIView{
    
    var identifier:String?
    
    func design(){}
    func setup(){}
    func setupViews(){}
    
    override func layoutSubviews() {
        super.layoutSubviews()
        design()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CollectionViewDelegateView: View, CollectionViewDelegates {
    
    dynamic func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    dynamic func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
    
}

class Cell: UICollectionViewCell {
    
    var model: Model?
    
    class var width: CGFloat {
        return 0
    }
    class var height: CGFloat {
        return 0
    }
    
    class func sizeFor(index: Int, inRect: CGRect)-> CGSize{
        return CGSize.zero
    }
    
    func setup(){}
    func design(){}
    
    override func layoutSubviews() {
        super.layoutSubviews()
        design()
        
    }
    
    required init?(coder: NSCoder){
        super.init(coder: coder)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
}

class ModelCell<M: Model>: Cell{
    
    override var model: Model?{
        get{
            return item
        }
        set{
            item = newValue as? M
        }
        
    }
    
    var item: M?{
        didSet{
            
        }
    }
}

class CollectionViewDelegateCell: Cell, CollectionViewDelegates{
    
    dynamic func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    dynamic func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
}

//MARK: - Constraints
extension UIView{
    
    func alignToSuperView(top tConstant: CGFloat? = nil,
                          leading lConstant: CGFloat? = nil,
                          trailing rConstant: CGFloat? = nil,
                          bottom bConstant: CGFloat? = nil,
                          width wConstant: CGFloat? = nil,
                          height hConstant: CGFloat? = nil,
                          centerX xConstant: CGFloat? = nil,
                          centerY yConstant: CGFloat? = nil,
                          equalWidthRatio ewMultiplier: CGFloat? = nil,
                          equalHeightRatio ehMultiplier: CGFloat? = nil,
                          fillWithPadding fPadding: CGFloat? = nil,
                          fillVerticallyPadding fvPadding: CGFloat? = nil,
                          fillHorizontallyPadding fhPadding: CGFloat? = nil){
        
        translatesAutoresizingMaskIntoConstraints = false
        guard let superview = superview else{return}
        
        if tConstant != nil{
            topAnchor.constraint(equalTo: superview.topAnchor, constant: tConstant!).isActive = true
        }
        if lConstant != nil{
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant:lConstant!).isActive = true
        }
        if rConstant != nil{
            trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -rConstant!).isActive = true
        }
        
        if bConstant != nil{
            bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -bConstant!).isActive = true
        }
        if wConstant != nil{
            widthAnchor.constraint(equalToConstant: wConstant!).isActive = true
        }
        if hConstant != nil{
            heightAnchor.constraint(equalToConstant: hConstant!).isActive = true
        }
        
        if xConstant != nil{
            centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: xConstant!).isActive = true
        }
        if yConstant != nil{
            centerYAnchor.constraint(equalTo: superview.centerYAnchor, constant: yConstant!).isActive = true
        }
        
        if ewMultiplier != nil{
            widthAnchor.constraint(equalTo: superview.widthAnchor, multiplier: ewMultiplier!).isActive = true
        }
        
        if ehMultiplier != nil{
            heightAnchor.constraint(equalTo: superview.heightAnchor, multiplier: ehMultiplier!).isActive = true
        }
        
        if fPadding != nil{
            alignToSuperView(top: fPadding, leading: fPadding, trailing: fPadding, bottom: fPadding)
        }
        
        if fvPadding != nil{
            alignToSuperView(top: fvPadding, bottom: fvPadding)
        }
        
        if fhPadding != nil{
            alignToSuperView(leading: fhPadding, trailing: fhPadding)
        }
    }
    
}

