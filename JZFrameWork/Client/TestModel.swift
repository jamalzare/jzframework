//
//  TestModel.swift
//  JZFrameWork
//
//  Created by Jamal on 5/4/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class TestModel: Model {
    var name: String?
    var family: String?
    var payment: String?
    
    required init() {
        super.init()
    }
    
    
    required init(json: JSON) {
        super.init(json: json)
        name = json["name"].stringValue
        family = json["family"].stringValue
        payment = json["payment"].stringValue
    }
}
