//
//  TestViewController.swift
//  JZFrameWork
//
//  Created by Jamal on 5/4/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class TestViewController: ViewModelCollectionViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewModel = CollectionViewModel()
        viewModel.cellClass = TestCell.self
        
        let model1: TestModel = {
            let m = TestModel()
            m.name = "jamal"
            m.family = "zare"
            m.payment = "50k $"
            return m
        }()
        
        let model2: TestModel = {
            let m = TestModel()
            m.name = "kamal"
            m.family = "zare"
            m.payment = "150k $"
            return m
        }()
        
        viewModel.list = [model1, model2]
        super.viewDidAppear(animated)
       
    }
    
}
