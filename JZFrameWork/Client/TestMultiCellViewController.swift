//
//  TestMultiCellViewController.swift
//  JZFrameWork
//
//  Created by Jamal on 5/6/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

protocol TestMultiCellViewControllerDelegate {
    func changeName()
}

class TestMultiCellViewController: MultiCellViewModelCollectionViewController {
    
    let viewModel1 = createViewModelFor(TestCell.self)
    let viewModel2 = createViewModelFor(TestCell2.self)
    let viewModel3 = createViewModelFor(TestStaticCell.self)
    let viewModel4 = createViewModelFor(TestCell2.self)
    
    var delegate: TestMultiCellViewControllerDelegate!{
        didSet{
            delegate.changeName()
        }
    }
    
    override func onDequeuingCell(_ cell: BaseCell, cellForItemAt indexPath: IndexPath) {
       
        if cell is TestCell2{
            self.delegate = (cell as! TestCell2)
            return
        }
        
        if cell is TestCell{
            let testCell = cell as! TestCell
            testCell.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addViewModels(viewModel1, viewModel3, viewModel2, viewModel4)
        setListForViewModels()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

extension TestMultiCellViewController: TestCellDelagate{
    
    func changeTitle() {
        print("title Changed")
    }
}

extension TestMultiCellViewController{
    
    func setListForViewModels(){
        let model1: TestModel = {
            let m = TestModel()
            m.name = "jamal"
            m.family = "zare"
            m.payment = "50k $"
            return m
        }()
        
        let model2: TestModel = {
            let m = TestModel()
            m.name = "kamal"
            m.family = "zare"
            m.payment = "150k $"
            return m
        }()
        
        let model3: TestModel2 = {
            let m = TestModel2()
            m.job = "kolsoom"
            m.company = "zare"
            m.salary = "6000k $"
            return m
        }()
        
        let model4: TestModel2 = {
            let m = TestModel2()
            m.job = "developer"
            m.company = "zare"
            m.salary = "100000k $"
            return m
        }()
        
        viewModel1.list = [model1, model2]
        viewModel2.list = [model3, model4]
        viewModel3.list = [Model()]
        viewModel4.list = [model3]
    }
    
}

