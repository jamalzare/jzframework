//
//  TestCell.swift
//  JZFrameWork
//
//  Created by Jamal on 5/4/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

protocol TestCellDelagate {
    func changeTitle()
}

class TestCell: BaseCell {
    
    var delegate: TestCellDelagate?
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var familyLabel: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!
    
    override func setModel(_ model: Model){
        let m = model as! TestModel
        nameLabel.text = m.name
        familyLabel.text = m.family
        amountLabel.text = m.payment
        delegate?.changeTitle()
    }
    
    override class func sizeFor(indexPath index: IndexPath, inSize size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: 120)
    }
}

