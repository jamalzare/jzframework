//
//  TestStaticCell.swift
//  JZFrameWork
//
//  Created by Jamal on 5/7/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class TestStaticCell: BaseCell {

    override class func sizeFor(indexPath index: IndexPath, inSize size: CGSize) -> CGSize {
        return CGSize(width: 200, height: 120)
    }

}
