//
//  TestCell2.swift
//  JZFrameWork
//
//  Created by Jamal on 5/6/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class TestCell2: BaseCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var familyLabel: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!
    
    override func setModel(_ model: Model){
        let m = model as! TestModel2
        nameLabel.text = m.job
        familyLabel.text = m.company
        amountLabel.text = m.salary
    }
    
    override class func sizeFor(indexPath index: IndexPath, inSize size: CGSize) -> CGSize {
        return CGSize(width: 300, height: 400)
    }

}

extension TestCell2: TestMultiCellViewControllerDelegate{

    func changeName(){
        print("name has been changed")
    }
}
