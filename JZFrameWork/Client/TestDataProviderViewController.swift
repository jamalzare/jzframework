//
//  TestDataProviderViewController.swift
//  JZFrameWork
//
//  Created by Jamal on 5/9/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class TestDataProviderViewController: ViewModelWithDataProviderViewControlle {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel1 = createViewModelFor(TestCell.self)
        addViewModels(viewModel1)
        provideData(request: AppRequests.getProducts(id: "23", name: "jamal"){ dto in
            viewModel1.list = dto.list
        })
    }
}
