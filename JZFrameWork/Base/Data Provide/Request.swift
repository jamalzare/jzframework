//
//  Request.swift
//  JZFrameWork
//
//  Created by Jamal on 5/9/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation


enum ResponseType {
    case operation
    case model
    case list
}

enum ResourceType{
    case api
    case mockData
}

protocol IRequest{
    
    associatedtype model: Model
    //typealias model = Model
    var api: String{get set}
    var method: String{get set}
    var responseType: ResponseType {get set}
    var parameters: [String: Any]{get set}
    var arrayParam:[Any]{get set}
    var dto: DTO<model> {get set}
    var callBack: (DTO<model>)-> Void {get set}
    var json: JSON{get set}
}

class Request<T: Model>: IRequest{
    
    var dto: DTO<T> = DTO()
    var parameters: [String : Any] = [:]
    var arrayParam: [Any] = []
    var json: JSON = []

    var api: String = ""
    var method: String = "get"
    var responseType: ResponseType = .operation
    var callBack: (DTO<T>)-> Void = {_ in }
    
    init() {
        
    }
    
    func responseCallBack(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            self.callBack(self.dto)
        })
        
        
    }
    
    func execute(){
        let json = JSONReader.Read()
        dto = DTO<T>(success: true, json: json, responseType: .list)
        responseCallBack()
    }
    
}

