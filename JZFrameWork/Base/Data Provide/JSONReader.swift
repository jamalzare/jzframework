//
//  JSONReader.swift
//  JZFrameWork
//
//  Created by Jamal on 5/9/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation


class JSONReader {
    
    static func Read()-> JSON{
        var json = JSON.null
        let url = Bundle.main.url(forResource: "MockJson",  withExtension: "json")
        
        if url == nil{
            print("could not find the file")
            return json
        }
        
        do{ json = try JSON( Data(contentsOf: url!)) }
        catch{ print("cant read file") }
        return json
    }
}
