//
//  DataProvider.swift
//  JZFrameWork
//
//  Created by Jamal on 5/9/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation

class DataProvider{
    
    var preloader = false
    
    func execute<T>(request: Request<T>){
        preloader = true
        print("preloader is starting")
        
        let callBack = request.callBack
        request.callBack = { dto in
            self.preloader = false
            print("preloader disabled")
            callBack(dto)
        }
        request.execute()
    }
    
    
    init() {
        
    }
    
}

//class DataProvider<T> where T : Model{
//
//    var preloader = false
//    var request: Request<T>!
//
//    func execute(){
//        preloader = true
//        print("preloader is starting")
//
//        let callBack = request.callBack
//        request.callBack = { dto in
//            self.preloader = false
//            print("preloader disabled")
//            callBack(dto)
//        }
//        request.execute()
//    }
//
//
//    init() {
//
//    }
//
//}

