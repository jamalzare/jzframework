//
//  DTO.swift
//  JZFrameWork
//
//  Created by Jamal on 5/9/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation


class  DTO<T: IModel> { // Data Transfer Object
    
    var success :Bool = false
    var status :Bool = false
    var model : T?
    var list = [T]()
    var message: String?
    var statusCode:Int?
    var httpStatusCode: Int?
    
    init(){}
    
    
    // how to check request status???
    private func checkStatus(json: JSON)-> Bool{
        return !(json["error"].exists() && json["statusCode"].exists() && json["message"].exists())
    }
    
    func initResponse(responseType: ResponseType, json:JSON){
        
        if responseType == .model{
            model =  T(json: json)
            
        }else if responseType == .list {
            for  (_, js) in json["list"]{
                list.append(T(json: js))
            }
        }
    }
    
    init(success:Bool, json: JSON, responseType: ResponseType) {
        
        var data = json
        if json["data"].exists(){
            data = json["data"]
        }
        self.success = success
        status = success ? checkStatus(json: data): false
        statusCode = data["statusCode"].intValue
        message = data["message"].stringValue
        initResponse(responseType: responseType, json: data)
    }
}

