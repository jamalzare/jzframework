//
//  RequestTypes.swift
//  JZFrameWork
//
//  Created by Jamal on 5/10/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation

class getListRequest<T:Model>: Request<T> {
    init(api: String, params: [String:String]) {
        super.init()
        method = "get"
        responseType = .list
        self.api = api
        self.parameters = params
    }
}

//class getModelRequest: Request<Model> {
//    override init() {
//        super.init()
//        method = "get"
//        responseType = .model
//    }
//}
//
//class PostOperationRequest: Request<Model> {
//    override init() {
//        super.init()
//        method = "post"
//        responseType = .operation
//    }
//}

//class PostListRequest: Request<Model> {
//    override init() {
//        super.init()
//        method = "post"
//        responseType = .list
//    }
//}

class AppRequests {
    

    static func getProducts(id:String, name: String, callback:@escaping (DTO<TestModel>)-> Void)-> Request<TestModel>{
        
        
        let params = [
            "id": id,
            "name": name
        ]
        let request = getListRequest<TestModel>(api: "api/somthing", params: params)
        request.callBack = callback
        
        return request
    }
}


