//
//  BaseCollectionViewController.swift
//  JZFrameWork
//
//  Created by Jamal on 5/4/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class BaseCollectionViewController: UIViewController {
    
    let padding: CGFloat = 12
    
    let collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        return cv
    }()
    
    func setCollectionView(){
        
        collectionView.backgroundColor = UIColor.appLightGreen
        collectionView.contentInset = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        
        
    }
    
    private func setConstraints(){
        view.addSubview(collectionView)
        collectionView.alignToSuperView(top: 0, leading: 0, trailing: 0, bottom: 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        setCollectionView()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setConstraints()
    }
    
}




