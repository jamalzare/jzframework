//
//  BaseCell.swift
//  JZFrameWork
//
//  Created by Jamal on 5/4/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

protocol BaseCellDelegate {
    
}

protocol ICell {
    var cellNib: UINib{get}
    var cellIdentifier: String {get}
}

class BaseCell: UICollectionViewCell, ICell {
    
    var cellNib: UINib{
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    var cellIdentifier: String{
        return String(describing: self)
    }
    
    class func sizeFor(indexPath index: IndexPath, inSize size: CGSize)-> CGSize{
        return CGSize.zero
    }
    
    func setModel(_ model: Model){}
    
    func setup(){}
    func style(){}
    
    override func layoutSubviews() {
        super.layoutSubviews()
        style()
        
    }
    
    required init?(coder: NSCoder){
        super.init(coder: coder)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
}
