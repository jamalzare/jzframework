//
//  MultiCellViewModelCollectionViewController.swift
//  JZFrameWork
//
//  Created by Jamal on 5/6/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

//Challenges
/*
 can not define cellClass and model Type in constractor like CollectionViewModel<TestCell, TestModel>() it has an error like cannot convert CollectionViewModel<TestCell, TestModel>() to  CollectionViewModel<BaseCell, Model>()
 
 */
import UIKit

class MultiCellViewModelCollectionViewController: ViewModelCollectionViewController {
    
    var viewModels: [ICollectionViewModel] = []
    
    class func createViewModelFor(_ cellModel: BaseCell.Type)-> CollectionViewModel{
        return CollectionViewModel(cellModel: cellModel)
    }
    
    func createViewModelFor(_ cellModel: BaseCell.Type)-> CollectionViewModel{
        return CollectionViewModel(cellModel: cellModel)
    }
    
    func addViewModels(_ _viewModels: ICollectionViewModel...){
        
        for i in 0..._viewModels.count-1{
            var model = _viewModels[i]
            viewModels.append(model)
            model.collectionView = collectionView
        }
        
        registerCells()
        
    }
    
    func createAndAddViewModelFor(cellModel: BaseCell.Type)-> CollectionViewModel{
        let viewModel = CollectionViewModel.init(cellModel: cellModel)
        viewModels.append(viewModel)
        viewModel.collectionView = collectionView
        return viewModel
    }
    
    
    override func registerCells() {
        for model in viewModels{
            collectionView.register(model.cellNib, forCellWithReuseIdentifier: model.cellIdentifier)
        }
    }
    
    func onDequeuingCell(_ cell: BaseCell, cellForItemAt indexPath: IndexPath ){}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

//--MARK: Collection View delegates
extension MultiCellViewModelCollectionViewController{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModels.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels[section].list.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = viewModels[indexPath.section].cellIdentifier
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: (identifier), for: indexPath) as! BaseCell

        onDequeuingCell(cell, cellForItemAt: indexPath)

        let model = viewModels[indexPath.section].list[indexPath.item]
        cell.setModel(model)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let maxSize = CGSize(width: collectionView.frame.width - (padding * 2), height: collectionView.frame.height)
        
        let size = viewModels[indexPath.section].cellClass.sizeFor(indexPath: indexPath, inSize: maxSize)
        return size
    }
    
}
