//
//  Model.swift
//  JZFrameWork
//
//  Created by Jamal on 5/4/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation

protocol IModel{
    init(json:JSON)
    init()
}
class Model: IModel {
    
    required init() {
        
    }
    
    required init(json: JSON) {
        
    }
    
}
