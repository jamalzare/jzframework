//
//  CollectionViewModel.swift
//  JZFrameWork
//
//  Created by Jamal on 5/4/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

/*
//Challenges:
 1- provide data
 2- define cells
 3- relate data, collectionView and cell and ViewContorller
 
 5- class CollectionViewModel<T: BaseCell, M: UICollectionViewCell>
 6- modelCells: [T.Type] for multiple cells
 7- these class shuld be on class
 8- every cell model has own list
 */

import UIKit

protocol ICollectionViewModel{
    
    var cellClass: BaseCell.Type{get set}
    var list: [Model] {get set}
    var cellNib: UINib{get}
    var cellIdentifier: String{get}
    var collectionView: UICollectionView? {get set}
}


//Challenge //what defrence between self and Type?????
class CollectionViewModel: ICollectionViewModel{
    
    var cellClass: BaseCell.Type
    var collectionView: UICollectionView?
    
    var list: [Model] = [] {
        didSet{
            collectionView?.reloadData()
            
        }
    }
    
    var cellNib: UINib{
        return cellClass.nib
    }
    
    var cellIdentifier: String{
        return cellClass.identifier
    }
    
    init(cellModel: BaseCell.Type) {
        self.cellClass = cellModel
    }
    
    init() {
        list = []
        cellClass = BaseCell.self
    }
}

