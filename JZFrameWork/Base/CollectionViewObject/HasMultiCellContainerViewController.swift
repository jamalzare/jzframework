//
//  HasMultiCellContainerViewController.swift
//  JZFrameWork
//
//  Created by Jamal on 5/21/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class HasMultiCellContainerViewController: UIViewController {
    
    var viewModels: [ICollectionViewModel]!
    var multiCellContainer = MultiCellContainer()
    
    class func createViewModelFor(_ cellModel: BaseCell.Type)-> CollectionViewModel{
        return MultiCellContainer.createViewModelFor(cellModel)
    }
    
    func createViewModelFor(_ cellModel: BaseCell.Type)-> CollectionViewModel{
        return multiCellContainer.createViewModelFor(cellModel)
    }
    
    func addViewModels(_ _viewModels: ICollectionViewModel...){
        multiCellContainer.addViewModels(_viewModels)
    }
    
    func createAndAddViewModelFor(cellModel: BaseCell.Type)-> CollectionViewModel{
        return multiCellContainer.createAndAddViewModelFor(cellModel: cellModel)
        
    }
    
    func onDequeuingCell(_ cell: BaseCell, cellForItemAt indexPath: IndexPath ){
        multiCellContainer.onDequeuingCell(cell, cellForItemAt: indexPath)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModels = multiCellContainer.viewModels
        multiCellContainer.setSuperView(view)
    }
    
}
