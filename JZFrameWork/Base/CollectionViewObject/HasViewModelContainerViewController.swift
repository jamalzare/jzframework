//
//  HasViewModelContainerViewController.swift
//  JZFrameWork
//
//  Created by Jamal on 5/21/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class HasViewModelContainerViewController: UIViewController {
    
    
    var viewModel: CollectionViewModel!
    let viewModelContainer = ViewModelContainer()
    
    func setViewModel(_ viewModel: CollectionViewModel){
        self.viewModel = viewModel
        viewModelContainer.setViewModel(self.viewModel)
        viewModelContainer.registerCells()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModelContainer.setSuperView(view)
        viewModel = viewModelContainer.viewModel
    }
    
}
