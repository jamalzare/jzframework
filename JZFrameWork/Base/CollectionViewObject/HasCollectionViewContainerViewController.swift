//
//  HasCollectionViewContainerViewController.swift
//  JZFrameWork
//
//  Created by Jamal on 5/21/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class HasCollectionViewContainerViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let obj = CollectionViewContainer()
        obj.setSuperView(self.view)
    }
}
