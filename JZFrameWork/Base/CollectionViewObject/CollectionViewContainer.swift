//
//  CollectionViewContainer.swift
//  JZFrameWork
//
//  Created by Jamal on 5/21/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

class CollectionViewContainer: UIView {
    
    let edgeInsets: CGFloat = 12
    
    let collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        return cv
    }()
    
    func setCollectionView(){
        collectionView.backgroundColor = UIColor.appLightGreen
        collectionView.contentInset = UIEdgeInsets(top: edgeInsets, left: edgeInsets, bottom: edgeInsets, right: edgeInsets)
        
    }
    
    private func setConstraints(){
        addSubview(collectionView)
        collectionView.alignToSuperView(top: 0, leading: 0, trailing: 0, bottom: 0)
    }
    
    func setSuperView(_ superView: UIView){
        superView.addSubview(self)
        alignToSuperView(top: 0, leading: 0, trailing: 0, bottom: 0)
    }
    
    func setup(){
        setConstraints()
        setCollectionView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

