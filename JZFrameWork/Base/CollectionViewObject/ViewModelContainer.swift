//
//  ViewModelContainer.swift
//  JZFrameWork
//
//  Created by Jamal on 5/21/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit


class ViewModelContainer: CollectionViewContainer {
    
    var viewModel: CollectionViewModel!
    
    func setViewModel(_ viewModel: CollectionViewModel){
        self.viewModel = viewModel
        collectionView.register(viewModel.cellNib, forCellWithReuseIdentifier: viewModel.cellIdentifier)
    }
    
    func registerCells(){
        
    }
    
    override func setCollectionView() {
        super.setCollectionView()
        collectionView.delegate = self
        collectionView.dataSource = self
        registerCells()
    }
    
}

extension ViewModelContainer: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: (viewModel.cellClass.identifier), for: indexPath) as! BaseCell
        cell.setModel(viewModel.list[indexPath.item])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let maxSize = CGSize(width: collectionView.frame.width - (edgeInsets * 2), height: collectionView.frame.height)
        
        let size = viewModel.cellClass.sizeFor(indexPath: indexPath, inSize: maxSize)
        return size
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

