//
//  ViewModelWithDataProviderViewControlle.swift
//  JZFrameWork
//
//  Created by Jamal on 5/9/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit

// for ViewControllers that they dont have CollectionView
class DataProviderViewController: UIViewController {
    
}

// for ViewController that have UICollectioView
class ViewModelWithDataProviderViewControlle: MultiCellViewModelCollectionViewController{
    
    func provideData<T>(request: Request<T>){
        let dataProvider = DataProvider()
        dataProvider.preloader = false
        dataProvider.execute(request: request)
    }
    
    
}


